#!/bin/bash -e

libdir=/usr/lib
. /usr/share/grub/grub-mkconfig_lib

set_blue_theme()
{
  cat << EOF
set menu_color_normal=cyan/blue
set menu_color_highlight=white/blue
EOF
}

# check for usable backgrounds
use_bg=false
  for i in /etc/bootsplash/themes/current/images/silent-$( echo $GRUB_GFXMODE|cut -dx -f1-2).{png,tga,jpg} /boot/grub2/themes/images/background-grub.{png,tga,jpg} ; do
    if is_path_readable_by_grub $i ; then 
      bg=$i
      case ${bg} in
        *.png)          reader=png ;;
        *.tga)          reader=tga ;;
        *.jpg|*.jpeg)   reader=jpeg ;;
      esac
      if test -e /boot/grub/i386-pc/${reader}.mod ; then
        echo "Found background image: `basename ${bg}`" >&2
        use_bg=true
        break
      fi
    fi
  done
#fi

# set the background if possible
if ${use_bg} ; then
  prepare_grub_to_access_device `${grub_probe} --target=device ${bg}`
  cat << EOF
insmod ${reader}
if background_image `make_system_path_relative_to_its_root ${bg}` ; then
  set color_normal=white/black
  set color_highlight=magenta/black
else
EOF
fi

# otherwise, set the traditional Debian blue theme
if ${use_bg} ; then
  set_blue_theme | sed -e "s/^/  /g"
  echo "fi"
else
  set_blue_theme
fi
