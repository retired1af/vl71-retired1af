#!/bin/sh

# Copyright 2006-2010  Robby Workman, Northport, AL, USA
# Copyright 2007-2010  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SCRIPT IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SCRIPT, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

DLNAME=gnupg
NAME=gnupg2
VERSION=${VERSION:-2.0.21}
LINK=${LINK:-"ftp://ftp.gnupg.org/gcrypt/gnupg/$DLNAME-$VERSION.tar.bz2"}
NUMJOBS=${NUMJOBS:-" -j7 "}

#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"2"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-"slapt-get libgpg-error openssl"} #Add deps needed TO BUILD this package here.
#----------------------------------------------------------------------------

# DO NOT EXECUTE if NORUN is set to 1
if [ "$NORUN" != "1" ]; then

CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-$NAME

#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------
#get the source..........
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
rm -rf $PKG
mkdir -p $TMP $PKG

cd $TMP
rm -rf gnupg-$VERSION
tar xvf $CWD/gnupg-$VERSION.tar.?z* || exit 1
cd gnupg-$VERSION

chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --sysconfdir=/etc \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --mandir=/usr/man \
  --infodir=/usr/info \
  --docdir=/usr/doc/gnupg2-$VERSION \
  --program-prefix="" \
  --program-suffix="" \
  --build=$CONFIGURE_TRIPLET \
  $CONFIG_OPTIONS || exit 1

make $NUMJOBS || exit 1
make install DESTDIR=$PKG || exit 1

# Create directory for global gpg.conf:
mkdir -p $PKG/etc/gnupg

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

( cd $PKG/usr/man
  find . -type f -exec gzip -9 {} \;
  for i in $(find . -type l) ; do ln -s $(readlink $i).gz $i.gz ; rm $i ; done
)

rm -f $PKG/usr/info/dir
gzip -9 $PKG/usr/info/*

mkdir -p $PKG/usr/doc/gnupg2-$VERSION
cp -a \
  ABOUT-NLS AUTHORS COPYING* INSTALL NEWS README* THANKS TODO VERSION \
  $PKG/usr/doc/gnupg2-$VERSION
# Build html docs
( cd doc
  make gnupg.html
  mv gnupg.html $PKG/usr/doc/gnupg2-$VERSION/html
)
# Fix some filenames in the html directory
( cd $PKG/usr/doc/gnupg2-$VERSION/html
mv how_002dto_002dspecify_002da_002duser_002did.html howto_specify_user_id.html
mv GnuPG_002d1-and-GnuPG_002d2.html GnuPG_1-and-GnuPG_2.html
mv gpg_002dpreset_002dpassphrase.html gpg_preset_passphrase.html
mv gpgsm_002dgencert_002esh.html gpgsm_gencert_sh.html
mv Invoking-gpg_002dpreset_002dpassphrase.html Invoking-gpg_preset_passphrase.html
mv Invoking-gpg_002dconnect_002dagent.html Invoking-gpg_connect_agent.html
mv gpg_002dconnect_002dagent.html gpg_connect_agent.html
mv Agent-GET_005fCONFIRMATION.html Agent-GET_CONFIRMATION.html
mv option-_002d_002denable_002dssh_002dsupport.html option-enable_ssh_support.html
mv option-_002d_002doptions.html option-options.html
mv Invoking-GPG_002dAGENT.html Invoking-GPG_AGENT.html
mv gpg_002dzip.html gpg_zip.html
mv option-_002d_002dp12_002dcharset.html option-p12_charset.html
mv option-_002d_002dallow_002dmark_002dtrusted.html option-allow_mark_trusted.html
mv Controlling-gpg_002dconnect_002dagent.html Controlling-gpg_connect_agent.html
mv Agent-GET_005fPASSPHRASE.html Agent-GET_PASSPHRASE.html
mv option-_002d_002dhomedir.html option-homedir.html
mv PKCS_002315-Card.html PKCS-15-Card.html
mv option-_002d_002dexport_002downertrust.html option-export_ownertrust.html
)
# Move html doc to the proper location
mv $PKG/usr/doc/gnupg2-$VERSION/faq.html $PKG/usr/doc/gnupg2-$VERSION/html/

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/${NAME}-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

rm -rf $TMP
fi
