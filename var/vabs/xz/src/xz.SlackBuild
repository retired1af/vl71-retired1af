#!/bin/sh

# Copyright 2009, 2010, 2011  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.

#   Permission to use, copy, modify, and distribute this software for
#   any purpose with or without fee is hereby granted, provided that
#   the above copyright notice and this permission notice appear in all
#   copies.
#
#   THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
#   WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#   MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
#   IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
#   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
#   LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
#   USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#   ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
#   OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
#   OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
#   SUCH DAMAGE.

NAME="xz"
VERSION=${VERSION:-"5.0.4"}
VER=$(echo $VERSION|sed 's/-/_/') #this fixes - in version
VL_PACKAGER=${VL_PACKAGER:-"M0E-lnx"}   #Enter your Name!
LINK=${LINK:-"http://tukaani.org/${NAME}/${NAME}-${VERSION}.tar.xz"}  #Enzter URL for package here!

#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-""} #Add deps needed TO BUILD this package here.
#----------------------------------------------------------------------------

# DO NOT EXECUTE if NORUN is set to 1
if [ "$NORUN" != "1" ]; then

#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------

PKGNAM=${NAME}
CWD=$(pwd)
RELEASEDIR=${CWD}/..
# Temporary build location.  This should *NOT* be a directory
# path a non-root user could create later...
TMP=${TMP:-/xz-tmp-$(mcookie)}
PKG=$TMP/package-$PKGNAM
rm -rf $PKG
mkdir -p $TMP $PKG

# Get the source code
for src in $(echo $LINK); do
	(
	cd $CWD
	wget -c --no-check-certificate $src
	)
done

cd $TMP
rm -rf $PKGNAM-$VERSION
tar xvf $CWD/$PKGNAM-$VERSION.tar.* || exit 1
cd $PKGNAM-$VERSION
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

CFLAGS="$SLKCFLAGS" \
CXXFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --docdir=/usr/doc/xz-$VERSION \
  --sysconfdir=/etc \
  --mandir=/usr/man \
  --enable-shared \
  --disable-static \
  --disable-rpath \
  --program-prefix= \
  --program-suffix= \
  --build=$CONFIGURE_TRIPLET \
  $CONFIG_OPTIONS || exit 1

make || j6  || exit 1
make DESTDIR=$PKG install || exit 1

# It might be advisable to have the libraries in /lib${LIBDIRSUFFIX}:
mkdir -p $PKG/lib${LIBDIRSUFFIX}
mv $PKG/usr/lib${LIBDIRSUFFIX}/liblzma.so.* $PKG/lib${LIBDIRSUFFIX}
( cd $PKG/usr/lib${LIBDIRSUFFIX}
  rm -f liblzma.so
  ln -sf ../../lib${LIBDIRSUFFIX}/liblzma.so.? liblzma.so
)

# At least the xz binary should also be in /bin:
mkdir -p $PKG/bin
mv $PKG/usr/bin/xz $PKG/bin
( cd $PKG/usr/bin
  ln -sf ../../bin/xz .
)

# Strip binaries:
( cd $PKG
  find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-debug 2> /dev/null
  find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-debug 2> /dev/null
)

# Compress and link manpages, if any:
if [ -d $PKG/usr/man ]; then
  ( cd $PKG/usr/man
    for manpagedir in $(find . -type d -name "man*") ; do
      ( cd $manpagedir
        for eachpage in $( find . -type l -maxdepth 1) ; do
          ln -s $( readlink $eachpage ).gz $eachpage.gz
          rm $eachpage
        done
        gzip -9 *.?
      )
    done
  )
fi

mkdir -p $PKG/usr/doc/$PKGNAM-$VERSION
cp -a \
  ABOUT-NLS AUTHORS COPYING* INSTALL* NEWS PACKAGERS README* THANKS \
  $PKG/usr/doc/$PKGNAM-$VERSION

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/${PKGNAM}-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc
cat $CWD/slack-desc > $RELEASEDIR/slack-desc

cd $PKG
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.tgz"

/sbin/makepkg -l y -c n $RELEASEDIR/${PKGNAM}-${VERSION}-${ARCH}-${BUILD}.tgz
fi
