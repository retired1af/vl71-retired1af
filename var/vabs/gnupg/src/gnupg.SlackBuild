#!/bin/sh

# Copyright 2008, 2009, 2010, 2011  Patrick J. Volkerding, Sebeka, Minnesota, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
NAME=gnupg
VERSION=1.4.19
LINK=${LINK:-"ftp://ftp.gnupg.org/gcrypt/gnupg/$NAME-$VERSION.tar.bz2"}

#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"2"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-"slapt-get openssl"} #Add deps needed TO BUILD this package here.
#----------------------------------------------------------------------------

if [ "$NORUN" != 1 ]; then

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

NUMJOBS=${NUMJOBS:-" -j7 "}

CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-$NAME

if [ "$ARCH" = "i586" ]; then
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
elif [ "$ARCH" = "s390" ]; then
  CONFIGURE_TRIPLET="$ARCH-vlocity-linux"
  SLKCFLAGS="-O2"
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fPIC"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
else
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="$ARCH-vlocity-linux"
fi
#get the source..........
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
rm -rf $PKG
mkdir -p $PKG $PKG

cd $TMP
rm -rf gnupg-$VERSION
tar xvf $CWD/gnupg-$VERSION.tar.bz2 || exit 1
cd gnupg-$VERSION
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --infodir=/usr/info \
  --mandir=/usr/man \
  --build=$CONFIGURE_TRIPLET \
  $CONFIG_OPTIONS || exit 1

make $NUMJOBS || make || exit 1
make install DESTDIR=$PKG || exit 1

find $PKG | xargs file | grep -e "executable" -e "shared object" | grep ELF \
  | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

rm -f $PKG/usr/info/dir
gzip -9 $PKG/usr/info/*

gzip -9 $PKG/usr/man/*/*

mkdir -p $PKG/usr/doc/gnupg-$VERSION
cp -a \
  ABOUT-NLS AUTHORS BUGS COPYING* INSTALL NEWS PROJECTS README* THANKS TODO VERSION \
  $PKG/usr/doc/gnupg-$VERSION
if [ -r $PKG/usr/share/gnupg/FAQ ]; then
  ( cd $PKG/usr/doc/gnupg-$VERSION
    ln -sf /usr/share/gnupg/FAQ . )
fi

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/*-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

# Add slack-desc files:
mkdir -p $PKG/install
cat $CWD/slack-desc.gnupg > $PKG/install/slack-desc

# Build the package:
cd $PKG
requiredbuilder -v -y -s $RELEASEDIR $PKG
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

rm -rf $TMP

fi
